import {setToken,getToken} from '@/libs/util'
import {login,getUserInfoByToken} from '@/api'


export default{
    state:{
        token:getToken(),
        userInfo:null
    },
    mutations:{
        setToken:function(state,token){
            state.token=token;
            setToken(token);
        },
        setUserInfo:function(state,userInfo){
            state.userInfo=userInfo;
        }
    },
    actions:{
        handleLogin({ commit }, { userName, password }){
            userName = userName.trim()
            return new Promise((resolve, reject) => {
                login({
                userName,
                password
                }).then(res => {
                    const data = res.data
                    if(res.data.code==1){
                        commit('setToken', data.token)
                    }
                    resolve(res)
                }).catch(err => {
                   reject(err)
                })
            })
        },
        getUserInfoByToken({commit},token){
            return new Promise((resolve, reject) => {
                getUserInfoByToken(token).then(res => {
                    const data = res.data
                    commit('setUserInfo', data)
                    resolve(res)
                }).catch(err => {
                   reject(err)
                })
            })
        },
        loginOut({commit}){
            return new Promise((resolve,reject)=>{
                commit('setToken', "");
                resolve('');
            })
        }
    }
}