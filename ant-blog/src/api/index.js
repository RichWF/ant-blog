import axios from '@/libs/api.request'

export const login = ({ userName, password }) => {
    const data = {
      userName,
      password
    }
    return axios.request({
      url: '/login',
      data,
      method: 'post'
    })
}


export const getUserInfoByToken=(token)=>{
   return axios.request({
     url:'/getUserInfoByToken',
     params: {
      token: token
     },
     method:'get'
   })
}



export const getArtilceList=({pageIndex=1,pageSize=10})=>{
  return axios.request({
    url:'/getArtilceList',
    params: {
      pageIndex,
      pageSize
    },
    method:'get'
  })
}


export const getArtilceById=(id)=>{
  return axios.request({
    url: '/getArtilceById',
    data:{
      id
    },
    method: 'post'
  })
}