import Mock from 'mockjs'
import { login,getUserInfoByToken,getArtilceList,getArtilceById } from './user'

// 配置Ajax请求延时，可用来测试网络延迟大时项目中一些效果
Mock.setup({
    timeout: 2000
})

// 登录相关和获取用户信息
Mock.mock(/\/login/, login);
Mock.mock(/\/getUserInfoByToken/,getUserInfoByToken)
Mock.mock(/\/getArtilceList/,getArtilceList)
Mock.mock(/\/getArtilceById/,getArtilceById)