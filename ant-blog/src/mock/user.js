import {getParams} from '@/libs/util'
//模拟用户列表
const USER_MAP = {
   guest: {
      name: 'guest',
      password:'guest',
      user_id: '1',
      access: ['guest'],
      token: 'guest',
      avatar:'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
    },
    admin: {
      name: 'admin',
      password:"admin",
      user_id: '2',
      access: ['admin'],
      token: 'admin',
      avatar:'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
    }
}

const listData = []
for (let i = 0; i < 23; i++) {
  listData.push({
    id:i+1,
    href: 'https://vue.ant.design/',
    title: `ant design vue part ${i+1}`,
    authorName:'果酱林',
    pulishTime:'2019年08月11日',
    readCount:123,
    avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
    description: 'Ant Design, a design language for background applications, is refined by Ant UED Team.',
    content: 'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  })
}


export const login = req => {
    req = JSON.parse(req.body)
    if(USER_MAP[req.userName]){
      return {
        code:1,
        message:"登录成功",
        token: USER_MAP[req.userName].token
      }
    }else{
      return{
        code:0,
        message:"账号或密码错误"
      }
    }
}


export const getUserInfoByToken=req=>{
  let params=getParams(req.url);
  return USER_MAP[params["token"]];
}


export const getArtilceList=req=>{
  let params=getParams(req.url);
  return listData;
}


export const getArtilceById=req=>{
  req=JSON.parse(req.body);
  let id=req.id;
  let model=listData.find(t=>t.id==id);
  return model;
}