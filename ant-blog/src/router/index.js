const routes=[{
    name:'Home',
    path:'/',
    alias:'/home',
    meta:{
        keepAlive:true,
        title:"首页"
    },
    component:()=>import('@/pages/Home')
},{
    name:'Login',
    path:'/login',
    meta:{
       title:"登录"
    },
    component:()=>import('@/pages/Login')
},{
    name:'Detail',
    path:'/detail/:id',
    meta:{
       title:"详情"
    },
    props:true,
    component:()=>import('@/pages/Detail')
}]


export default routes;