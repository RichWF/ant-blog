const TOKEN="user_token";


 export const setToken=function(value){
     window.sessionStorage.setItem(TOKEN,value);
 }

 export const getToken=function(){
     const token=window.sessionStorage.getItem(TOKEN);
     if(token) return token;
     else return false;
 }


 /**
 * @param {String} url
 * @description 从URL中解析参数
 */
export const getParams = url => {
    const keyValueArr = url.split('?')[1].split('&')
    let paramObj = {}
    keyValueArr.forEach(item => {
      const keyValue = item.split('=')
      paramObj[keyValue[0]] = keyValue[1]
    })
    return paramObj
  }

///设置标题
export const setTitle=(title)=>{
    window.document.title=title;
  }