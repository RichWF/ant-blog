import HttpRequest from '@/libs/httpRequest.js'
const baseUrl = process.env.NODE_ENV === 'development' ? "/": "/"

const axios = new HttpRequest(baseUrl)
export default axios