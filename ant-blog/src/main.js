import Vue from 'vue'
import Antd from 'ant-design-vue'
import App from './App.vue'
import 'ant-design-vue/dist/antd.css'
import './assets/styles/reset.less'
import './assets/styles/common.less'
import routes from './router'
import VueRouter from 'vue-router'
import store from './store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import {setTitle} from './libs/util'

NProgress.configure({     
  easing: 'ease',  // 动画方式    
  speed: 2000,  // 递增进度条的速度    
  showSpinner: false, // 是否显示加载ico    
  trickleSpeed: 200, // 自动递增间隔    
  minimum: 0.3 // 初始化时的最小百分比
})



if (process.env.NODE_ENV !== 'production') require('@/mock')

Vue.config.productionTip = false
Vue.use(VueRouter);

const router = new VueRouter({
  mode:"history",
  routes // (缩写) 相当于 routes: routes
})


router.beforeEach((to, from , next) => {
  // 每次切换页面时，调用进度条
  NProgress.start();

  // 这个一定要加，没有next()页面不会跳转的。这部分还不清楚的去翻一下官网就明白了
  next();
});

router.afterEach((to, from) => {  
  // 在即将进入新的页面组件前，关闭掉进度条
  NProgress.done();
  if(to.meta.title){
    setTitle(to.meta.title);
  }
})

Vue.use(Antd)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
